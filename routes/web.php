<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'Main\MainController@inicio')->name('inicio');
Route::get('/otro', 'Main\MainController@otro')->name('otro');
//Route::get('/', 'Main\MainController@home_ini')->name('home');
